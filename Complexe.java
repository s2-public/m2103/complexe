
public class Complexe {
	
	private final float partieReelle;
	private final float partieImaginaire;

	public static final Complexe UN = new Complexe(1.0F,0.0F);
	public static final Complexe ZERO = new Complexe(0.0F,0.0F);
	
	/**
	 * construit un nombre complexe
	 * 
	 * @param partieReel
	 * 			partie réelle du nombre complexe
	 * @param partieImaginaire
	 * 			partie imaginaire du nombre complexe
	 */
	public Complexe(float partieReelle, float partieImaginaire){
		this.partieReelle=partieReelle;
		this.partieImaginaire=partieImaginaire;
	}
	
	public float getPartieReelle() {
		return this.partieReelle;
	}
	
	public float getPartieImaginaire() {
		return this.partieImaginaire;
	}
	
	public boolean estReel() {
		return this.partieImaginaire == 0.0F;
	}
	
	public boolean estImaginairePur() {
		return this.partieReelle == 0.0F;
	}
	
	public Complexe somme(Complexe c) {
		return new Complexe(this.partieReelle+c.getPartieReelle(),
				this.partieImaginaire+c.getPartieImaginaire());
	}
	
	public Complexe produit(Complexe c) {
		return new Complexe(this.partieReelle *c.getPartieReelle() - this.partieImaginaire * c.partieImaginaire,
				this.partieReelle * c.getPartieImaginaire()+c.getPartieReelle()*this.partieImaginaire);
	}
	
	public boolean egal(Complexe c) {
		return this.partieReelle == c.getPartieReelle() && this.partieImaginaire == c.getPartieImaginaire();
	}
	
	/**
	 * calcule le module d'un nombre complexe
	 * 
	 * @return module d'un nombre complexe
	 */
	public float module() {
		return (float) Math.sqrt(this.partieReelle*this.partieReelle+ this.partieImaginaire*this.partieImaginaire);
	}
	
	@Override
	public String toString() {
		if(this.partieImaginaire == 0.0F) {
			return "" + this.partieReelle;
		}
		if(this.partieImaginaire > 0.0F) {
			return this.partieReelle + " + " + this.partieImaginaire + "i";
		}
		return this.partieReelle + " - " + (-this.partieImaginaire + "i");
	}
}
