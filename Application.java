
public class Application {
	
	public static void main(String[] args) {
		
		Complexe c1 = new Complexe(3.0F, 2.0F);
		Complexe c2 = new Complexe(4.0F,-7.0F);
		Complexe c3 = new Complexe(5.0F,0.0F);
		
		System.out.println("Commutative");
		System.out.println(c1.produit(c2));
		System.out.println(c2.produit(c1));
		System.out.println("Associative");
		System.out.println(c1.produit(c2.produit(c3)));
		System.out.println(c1.produit(c2).produit(c3));
		System.out.println("Distributive");
		System.out.println(c1.produit(c2.somme(c3)));
		System.out.println((c1.produit(c2)).somme(c1.produit(c3)));
		System.out.println("Complexe 1 est l'element neutre");
		System.out.println(c1);
		System.out.println(c1.produit(Complexe.UN));
		System.out.println("Complexe 0 est l'element absorbant");
		System.out.println(c1);
		System.out.println(c1.produit(Complexe.ZERO));
	}
}
